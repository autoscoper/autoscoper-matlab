volume = 0;

%open Connection
autoscoper_socket = openConnection('127.0.0.1');

%load Trial
loadTrial(autoscoper_socket, 'D:\data\Autoscoper Development\test - Mayacam1 - Copy.cfg');

%Load tracking
loadTrackingData(autoscoper_socket, volume, 'D:\data\Autoscoper Development\tracking.tra');

%set Background
setBackground(autoscoper_socket,0.3);

%load filter settings (optional)
%loadFilters(autoscoper_socket,0,'D:\data\Autoscoper Development\Trial001\tracking_settings.cfg');
%loadFilters(autoscoper_socket,1,'D:\data\Autoscoper Development\Trial001\tracking_settings.cfg');

for frame=1:10
    %set frame
    setFrame(autoscoper_socket,frame);
    
    %get pose for the frame
    pose = getPose(autoscoper_socket,volume,frame);
    
    
    %pose layout is [trans_x trans_y trans_z yaw roll pitch]
    %Order of rotation is 'ZYX' and angles are expressed in degree,e.g.
    %eul2rotm([degtorad(pose(4))    degtorad(pose(5)) degtorad(pose(6))])
    %converts to a rotationmatrix
   
    %%%%%%%%%%% Start Optimization
        %run optimizer here and repeatidly get the ncc, if NaN then the
        %bone is out of view for the camera
        ncc = getNCC(autoscoper_socket,volume,pose)
        
        %optional read Images - Channel 1 is rad - Cannel 2 is drr -
        %Channel 3 is mask
        %I = readImages(autoscoper_socket,volume, 0 ,pose);
        %imshow(I,'InitialMagnification', 500);
        
    %%%%%%%%%%%End Optimization
             
    %At the end set the best pose
    setPose(autoscoper_socket,volume,frame,pose);
end

%If everything is done: Save tracking
saveTrackingData(autoscoper_socket,volume,'D:\data\Autoscoper Development\tracking_save.tra');
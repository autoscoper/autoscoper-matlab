function [A] = readImages(autoscoper_socket,volume,cameraID,pose)
    %READIMAGES Summary of this function goes here
    %  Detailed explanation goes here
    fwrite(autoscoper_socket,[10 typecast(int32(volume),'uint8') typecast(int32(cameraID),'uint8') typecast(double(pose),'uint8')]);
    while autoscoper_socket.BytesAvailable == 0
       pause(1)
    end
    data = fread(autoscoper_socket, 9);

    width = typecast(uint8(data(2:5)),'uint32');
    height = typecast(uint8(data(6:9)),'uint32');

    img = zeros(3*width*height,1,'uint8');
    byteRead = 0;
    while byteRead < width*height*3;
        toRead = autoscoper_socket.BytesAvailable;  
        if toRead > 0
            img(byteRead+1 : byteRead + toRead) = fread(autoscoper_socket, toRead);
        end
        byteRead = byteRead + toRead;
    end
    A = reshape(img,width,height,3);
    A = imrotate(A,90);
end


%% This Code Optimizes a frame or a couple of frames
path_root = 'C:\' % CHANGE THIS: Folder that you want to auto-save your optimization
tracking_filename = 'taskname_modelname.tra'; % CHANGE THIS: TRACKING FILENAME

% COMMENT IN IF YOU HAVE NOT ALREADY CONNECTED MATLAB TO AUTOSCOPER
% autoscoper_socket = openConnection('127.0.0.1');
% setBackground(autoscoper_socket,0.2);

% Which volume position do you want to optimize?
% volume = 1; 

% Look for the Best Answer Around Here
myLim = 5; % [5] or [10 2 8 2 4 3];

% PUT THE LIST OF FRAMES HERE:
frame_list = [0:12:200] % This can be a list or just one frame

opt_method = 1; % 1: PSO, 2: Simulate Annealing

combine_NCCs = 'Sum'; % 'SinglePlane' 'Sum' 'Mult'



%% Use What Frame to start Optimization
use_curFrame     = 1;

use_these_frames = 0; frame_1 = 46; frame_2 = 50; 

use_prevFrame    = 0; prevFrame = 165;


if use_curFrame == 1
    use_these_frames = 0;
    use_prevFrame    = 0;
end

%% Optimization Function Definition
if strcmp(combine_NCCs,'Mult')
    FUNC = @(pose) getNCC_Mult(pose,autoscoper_socket,volume);   
elseif strcmp(combine_NCCs,'Sum')
    FUNC = @(pose) getNCC_Sum(pose,autoscoper_socket,volume);   
elseif strcmp(combine_NCCs,'SinglePlane')
    FUNC = @(pose) getNCC_SinglePlane(pose,autoscoper_socket,volume);
end

if opt_method == 1
    options = optimoptions(@particleswarm,'FunctionTolerance',1e-3,'ObjectiveLimit',0,'MaxStallIterations',20, ...
        'HybridFcn','patternsearch','MinNeighborsFraction',0.05,'SwarmSize',250);
else
    options = optimoptions(@simulannealbnd,'FunctionTolerance',1e-5,'ObjectiveLimit',0,...
        'HybridFcn',@fmincon);
end


%%
nFrameList = length(frame_list);
opt_functions = [];
change_in_opt_pose = [];
change_in_opt_func = [];
NEW_tracking_save_path = fullfile(path_root,'Tracking',['autogen_' tracking_filename]);
tic;
for counter = 1:nFrameList
    %set frame
    setFrame(autoscoper_socket,frame_list(counter));
    
    %get pose for the frame
    if use_curFrame == 1
        pose0 = getPose(autoscoper_socket,volume,frame_list(counter));
    else
        if counter == 1
            pose0 = getPose(autoscoper_socket,volume,frame_list(counter));
        else
            pose0 = getPose(autoscoper_socket,volume,frame_list(counter-1));
            setPose(autoscoper_socket,volume,frame_list(counter),pose0);
        end
    end
    if nFrameList == 1 && use_prevFrame == 1
        pose0 = getPose(autoscoper_socket,volume,prevFrame);
        setPose(autoscoper_socket,volume,frame_list(counter),pose0);
    end
    if nFrameList == 1 && use_these_frames == 1
        pose_first = getPose(autoscoper_socket,volume,frame_1);
        pose_second = getPose(autoscoper_socket,volume,frame_2);
        pose0 = pose_second + (frame_list(counter)-frame_2)*(pose_second - pose_first)./(frame_2-frame_1);
        setPose(autoscoper_socket,volume,frame_list(counter),pose0);        
    end
    
    %     setPose(autoscoper_socket,volume,frame_list(counter),pose0);
    curVal = FUNC(pose0);
    
    lb = (pose0-myLim);
    ub = (pose0+myLim);
    nvars = 6;
    if opt_method == 1
        [opt_post,fval,exitflag,output] = particleswarm(FUNC,nvars,lb,ub,options);
    else
        [opt_post,fval,exitFlag,output] = simulannealbnd(FUNC,pose0,lb,ub,options);
    end
    pBest = opt_post;
    
    if fval > curVal % If we made it worst
        pBest = pose0;
    end
    
    opt_functions(counter,:) = [curVal, fval]
    
    %At the end set the best pose
    setPose(autoscoper_socket,volume,frame_list(counter),pBest);
    
    change_in_opt_pose(counter,:) = pBest - pose0
    change_in_opt_func(counter,:) = curVal - fval;
    % Displat
    disp(['Frame #',num2str(frame_list(counter)),' is done.']);
    
    saveTrackingData(autoscoper_socket,volume,NEW_tracking_save_path);
    toc;
end
toc;
f = msgbox('Operation Completed');

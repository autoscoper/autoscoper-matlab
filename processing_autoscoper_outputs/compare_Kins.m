%% User Interface
% main_path = 'P:\CMaldonadoRodas\Cadaver Knee\Cadaver Downhill Simplex\Bardiya_Tracked_PSO\';
% boneName = 'Femur';
clear;
% file_1_tra = fullfile('P:\iTWA_Instrumented_Total_Wrist\Subjects\CADAVER\WN00000\Autoscoper\Tracking','FlexExt_mc2_mc4.tra');
% file_2_tra = fullfile('P:\Eleanor\toDo\WN00000_Single_Plane\Tracking','SinglePlane_FlexExt_mc2_mc4.tra');
cadaver_path = 'P:\iTWA_Instrumented_Total_Wrist\Subjects\CADAVER\WN00000\Autoscoper\Tracking';
file_1_tra = fullfile(cadaver_path,'FlexExt_mc3.tra');
file_2_tra = fullfile(cadaver_path,'FlexExt_mc3_PSO.tra');


%% Read TRA File
bone_file1_16x1 = csvread(file_1_tra);
bone_file2_16x1 = csvread(file_2_tra);

counter = 1;
% bone_file1_6x1 = nan(101,6);
% bone_file2_6x1 = nan(101,6);
for iFrame = 1:600
    bone_file1_4x4{counter,1} = reshape(bone_file1_16x1(iFrame,1:16),[4 4])';
    bone_file2_4x4{counter,1} = reshape(bone_file2_16x1(iFrame,1:16),[4 4])';
    % In 3 translation and 3 rotation format
    bone_file1_6x1(counter,1:3) = bone_file1_4x4{counter,1}(1:3,4)';
    bone_file2_6x1(counter,1:3) = bone_file2_4x4{counter,1}(1:3,4)';
    bone_file1_6x1(counter,4:6) = rad2deg(tform2eul(bone_file1_4x4{counter,1}));
    bone_file2_6x1(counter,4:6) = rad2deg(tform2eul(bone_file2_4x4{counter,1}));
    
    % Find Relative to First Frame
    bone_file1_4x4_rel{counter,1} = bone_file1_4x4{counter,1} * transposefX4(bone_file1_4x4{1,1});
    bone_file2_4x4_rel{counter,1} = bone_file2_4x4{counter,1} * transposefX4(bone_file2_4x4{1,1});
    % In 3 translation and 3 rotation format
    bone_file1_6x1_rel(counter,1:3) = bone_file1_4x4_rel{counter,1}(1:3,4)';
    bone_file2_6x1_rel(counter,1:3) = bone_file2_4x4_rel{counter,1}(1:3,4)';
    bone_file1_6x1_rel(counter,4:6) = rad2deg(tform2eul(bone_file1_4x4_rel{counter,1}));
    bone_file2_6x1_rel(counter,4:6) = rad2deg(tform2eul(bone_file2_4x4_rel{counter,1}));

    counter = counter + 1;
end
figure(201);
plot(bone_file1_6x1-bone_file2_6x1)
figure(202);
plot(bone_file1_6x1_rel-bone_file2_6x1_rel)
mean(bone_file1_6x1_rel-bone_file2_6x1_rel,'omitnan')
std(bone_file1_6x1_rel-bone_file2_6x1_rel,'omitnan')

%%
% for i=1:6
%     figure(i);clf
%     plotregression(bone_file1_6x1(:,i),bone_file2_6x1(:,i))
%     figure(i+10);clf
%     plotregression(bone_file1_6x1_rel(:,i),bone_file2_6x1_rel(:,i))
% 
% end
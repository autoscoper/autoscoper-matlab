%% User Interface
% [path,models,taskName] = TWA_Header_Test(1,0); % 1 and 0 because we only need the reference model


path_file = 'C:\'; % Folder Path
path_name = 'neighbor_ncc_values.ncc'; % File path that you have saved with autoscoper

ncc_neighbor = fullfile(path_file,path_name);
%% Read NCC File
if isfile(ncc_neighbor)
    fileID = fopen(ncc_neighbor,'r');
    poseNCCs_all = fscanf(fileID,'%f,%f,%f,%f,%f,%f,%f,%f,%f\n');
    fclose(fileID);
    
    poseNCCs = reshape(poseNCCs_all,9,[])';
    
    NCCs = poseNCCs(:,7:9);

    % Plot
    figure(1); clf
    plot(NCCs(:,3),'DisplayName','NCCs');
    grid on;
    title('Normalized Cross-Correlation Values')
    ax = gca;
    ax.FontSize = 14;
else
    error('NCC Values are not saved...');
end
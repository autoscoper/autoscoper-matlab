%% Path for .ncc file
path_aut_nccs = 'PUT_YOUR_NCC_FILEPATH_HERE.ncc';

%% Read NCC File
if isfile(path_aut_nccs)
    fileID = fopen(path_aut_nccs,'r');
    NCCs_all = fscanf(fileID,'%f,%f,%f\n');
    fclose(fileID);
    
    NCCs = reshape(NCCs_all,3,[])';
    
    % Plot
    figure(1); clf
    plot(NCCs,'DisplayName','NCCs');
    grid on;
    legend('Camera 01','Camera 02','Final Cost Function')
    title('Normalized Cross-Correlation Values')
    ax = gca;
    ax.FontSize = 14;
else
    error('NCC Values are not saved...');
end